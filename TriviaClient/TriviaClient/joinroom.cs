﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class joinroom : Form
    {
        menu _returnTo;
        string _username;        
        string _roomName;
        string _questionNo;
        string _playersNo;
        string _time;
        Socket _socket;
        SortedDictionary<string, string> _roomAndId = null;

        public joinroom()
        {
            InitializeComponent();
        }

        public joinroom(menu m, string username, Socket sock)
        {
            _socket = sock;
            _returnTo = m;
            _username = username;
            _roomAndId = new SortedDictionary<string, string>();
            InitializeComponent();
        }

        //event - the button back is clicked
        private void back_Click(object sender, EventArgs e)
        {
            this.Close();
            _returnTo.Show();   //return to the menu
        }

        /*
        The function accepts a number and number of bytes, turns the number to String
        and adds zeros at the beginning of the number until the length of the string is equal to the desired number of bytes.
        Input:  number (int), number of bytes (int)
        Output: the new string
        */
        public string ValuePadding(int num, int bytes)
        {
            string str = num.ToString();
            while (str.Length < bytes)
            {
                str = "0" + str;
            }
            return str;
        }

        //event - the button joinRoom is clicked
        private void join_Click(object sender, EventArgs e)
        {
            byte[] reply = new byte[10000];
            string result = "";

            try
            {
                string msg = "209" + ValuePadding(Int32.Parse(_roomAndId[list.SelectedItem.ToString()]), 4);            
                _socket.Send(Encoding.UTF8.GetBytes(msg));
                _socket.Receive(reply);
                result = System.Text.Encoding.UTF8.GetString(reply);
            }
            catch (Exception ex)
            {
                Console.WriteLine("sending failed! " + ex.Message);
            }
            
            string code = result.Substring(0, 4);

            if (code == "1100") //if success
            {
                _questionNo = result.Substring(4, 2);
                _time = result.Substring(6, 2);

                //go to the form waitForGame
                waitForGame waitForGameForm = new waitForGame(_returnTo, _username, _roomName, _playersNo, _questionNo, _time, "", _socket);
                this.Close();
                waitForGameForm.Show();
            }
            else if (code == "1101")    //if the room is full
            {
                label2.Text = "failed - room is full";
            }
            else if (code == "1102")    //if failed
            {
                label2.Text = "failed - room not exist or other reason";
            }

            label2.Location = new Point((this.Width/2) - (label2.Size.Width / 2),label2.Location.Y);
        }

        private void joinroom_Load(object sender, EventArgs e)
        {                   
            byte[] reply = new byte[10000];
            user.Text = _username;
            string result = "";

            try
            {
                _socket.Send(Encoding.UTF8.GetBytes("205"));    //ask for the rooms' list
                _socket.Receive(reply);
                result = System.Text.Encoding.UTF8.GetString(reply);
            }
            catch (Exception ex)
            {
                Console.WriteLine("sending failed!" + ex.Message);
            }

            string roomName = "", id = "";
            int roomNameLen = 0;

            int numRooms = Int32.Parse(result.Substring(3,4));

            if (numRooms != 0)
            {
                //cut the message according to the protocol
                string rooms = (result.Substring(7));

                for (int i = 0; i < numRooms; i++)
                {
                    id = rooms.Substring(0, 4);
                    roomNameLen = Int32.Parse(rooms.Substring(4, 2));
                    roomName = rooms.Substring(6, roomNameLen);
                    list.Items.Add(roomName);   //add the room to the list
                    _roomAndId.Add(roomName, id);   //add the room to the dictionary
                    rooms = rooms.Substring(6 + roomNameLen);
                }
            }

            //Delete all empty entries in the list so that the user can not click them
            int count = list.Items.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                if (list.Items[i].ToString() == " ")
                {
                    list.Items.RemoveAt(i);
                }
            }

            list2.Enabled = false;  //so the user can not click on the list of users
        }

        //event - one of the rooms is selected
        private void list_SelectedIndexChanged(object sender, EventArgs e)
        {
            list2.Items.Clear();    //clean the users' list for case it's not empty

            byte[] reply = new byte[10000];
            string result = "";

            try
            {
                string msg = "207" + _roomAndId[list.SelectedItem.ToString()];  //ask for the users' list for the selected room          
                _socket.Send(Encoding.UTF8.GetBytes(msg));
                _socket.Receive(reply);
                result = System.Text.Encoding.UTF8.GetString(reply);
            }
            catch (Exception ex)
            {
                Console.WriteLine("sending failed!" + ex.Message);
            }

            string username = "";
            int usernameLen = 0;
            int numUsers = Int32.Parse(result[3].ToString());
            _playersNo = numUsers.ToString();

            if (numUsers != 0)
            {
                //cut the message according to the protocol
                string users = (result.Substring(4));

                for (int i = 0; i < numUsers; i++)
                {
                    usernameLen = Int32.Parse(users.Substring(0, 2));
                    username = users.Substring(2, usernameLen);
                    list2.Items.Add(username);  //add to the users' list
                    users = users.Substring(2 + usernameLen);
                }
            }
            
            _roomName = list.SelectedItem.ToString();
            join.Enabled = true;    //when a room is selected - the user can join this room
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            //clean all the lists and the dictionary
            list.Items.Clear();
            list2.Items.Clear();
            _roomAndId.Clear();
            joinroom_Load(sender, e);
        }

        private void list2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void joinroom_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
