﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class bestScores : Form
    {
        Socket _socket;
        string _msg;
        string _username;
        menu _returnTo;

        public bestScores()
        {
            InitializeComponent();
        }

        public bestScores(Socket sock, string response, menu m, string username)
        {
            _socket = sock;
            _msg = response.Substring(3);
            _returnTo = m;
            _username = username;
            InitializeComponent();
        }
        
        private void bestScores_Load(object sender, EventArgs e)
        {
            Label[] lbls = new Label[3];
            lbls[0] = this.first;
            lbls[1] = this.second;
            lbls[2] = this.third;

            //cut the message according to the protocol
            for(int i = 0; i<3; i++)
            {
                int len = Int32.Parse(_msg.Substring(0, 2));
                string name = _msg.Substring(2, len);
                string score = (Int32.Parse(_msg.Substring(2 + len, 6))).ToString();
                _msg = _msg.Substring(8 + len);
                if (len != 0)
                {
                    lbls[i].Text = name + " - " + score;
                }

                lbls[i].Location = new Point((this.Width / 2) - (lbls[i].Size.Width / 2), lbls[i].Location.Y);
            }
        }


        //event - the button back is clicked
        private void back_Click(object sender, EventArgs e)
        {
            this.Close();
            _returnTo.Show();   //return to the menu
        }
    }
}
