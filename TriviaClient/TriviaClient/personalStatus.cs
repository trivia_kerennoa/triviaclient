﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class personalStatus : Form
    {
        Socket _socket;
        string _msg;
        string _username;
        menu _returnTo;

        public personalStatus()
        {
            InitializeComponent();
        }

        public personalStatus(Socket sock, string msg, menu m,string user)
        {
            _username = user;
            _socket = sock;
            _msg = msg.Substring(3);
            _returnTo = m;
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void personalStatus_Load(object sender, EventArgs e)
        {
            //update all the information
            //cut the message according to the protocol
            user.Text = _username;
            numGames.Text = Int32.Parse(_msg.Substring(0, 4)).ToString();
            numRight.Text = Int32.Parse(_msg.Substring(4, 6)).ToString();
            numWrong.Text = Int32.Parse(_msg.Substring(10, 6)).ToString();
            timeAvg.Text = (Int32.Parse(_msg.Substring(16, 2))).ToString() + "." + _msg.Substring(18, 2);
        }

        //event - the button back is clicked
        private void back_Click(object sender, EventArgs e)
        {
            this.Hide();
            _returnTo.Show();   //return to the menu
        }
    }
}
