﻿namespace TriviaClient
{
    partial class menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menu));
            this.label1 = new System.Windows.Forms.Label();
            this.signin = new System.Windows.Forms.Button();
            this.username = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.signup = new System.Windows.Forms.Button();
            this.join = new System.Windows.Forms.Button();
            this.status = new System.Windows.Forms.Button();
            this.create = new System.Windows.Forms.Button();
            this.best = new System.Windows.Forms.Button();
            this.quit = new System.Windows.Forms.Button();
            this.user = new System.Windows.Forms.Label();
            this.hello = new System.Windows.Forms.Label();
            this.signout = new System.Windows.Forms.Button();
            this.error = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Gabriola", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(211, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 45);
            this.label1.TabIndex = 2;
            this.label1.Text = "Username:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // signin
            // 
            this.signin.BackColor = System.Drawing.Color.Transparent;
            this.signin.BackgroundImage = global::TriviaClient.Properties.Resources._39d43d061a91b5f0266479feb7bf4596;
            this.signin.Font = new System.Drawing.Font("Gabriola", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signin.Location = new System.Drawing.Point(717, 142);
            this.signin.Name = "signin";
            this.signin.Size = new System.Drawing.Size(110, 61);
            this.signin.TabIndex = 3;
            this.signin.Text = "Sign in";
            this.signin.UseVisualStyleBackColor = false;
            this.signin.Click += new System.EventHandler(this.signin_Click);
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username.Location = new System.Drawing.Point(311, 142);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(381, 29);
            this.username.TabIndex = 4;
            // 
            // password
            // 
            this.password.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password.Location = new System.Drawing.Point(311, 177);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(381, 29);
            this.password.TabIndex = 6;
            this.password.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Gabriola", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(215, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 45);
            this.label2.TabIndex = 5;
            this.label2.Text = "Password:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // signup
            // 
            this.signup.BackColor = System.Drawing.Color.Transparent;
            this.signup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("signup.BackgroundImage")));
            this.signup.Font = new System.Drawing.Font("Gabriola", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signup.Location = new System.Drawing.Point(416, 246);
            this.signup.Name = "signup";
            this.signup.Size = new System.Drawing.Size(178, 47);
            this.signup.TabIndex = 7;
            this.signup.Text = "Sign up";
            this.signup.UseVisualStyleBackColor = false;
            this.signup.Click += new System.EventHandler(this.button1_Click);
            // 
            // join
            // 
            this.join.BackColor = System.Drawing.Color.Transparent;
            this.join.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("join.BackgroundImage")));
            this.join.Enabled = false;
            this.join.Font = new System.Drawing.Font("Gabriola", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.join.Location = new System.Drawing.Point(416, 299);
            this.join.Name = "join";
            this.join.Size = new System.Drawing.Size(178, 47);
            this.join.TabIndex = 8;
            this.join.Text = "Join room";
            this.join.UseVisualStyleBackColor = false;
            this.join.Click += new System.EventHandler(this.join_Click);
            // 
            // status
            // 
            this.status.BackColor = System.Drawing.Color.Transparent;
            this.status.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("status.BackgroundImage")));
            this.status.Enabled = false;
            this.status.Font = new System.Drawing.Font("Gabriola", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status.Location = new System.Drawing.Point(416, 405);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(178, 47);
            this.status.TabIndex = 10;
            this.status.Text = "My status";
            this.status.UseVisualStyleBackColor = false;
            this.status.Click += new System.EventHandler(this.status_Click);
            // 
            // create
            // 
            this.create.BackColor = System.Drawing.Color.Transparent;
            this.create.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("create.BackgroundImage")));
            this.create.Enabled = false;
            this.create.Font = new System.Drawing.Font("Gabriola", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.create.Location = new System.Drawing.Point(416, 352);
            this.create.Name = "create";
            this.create.Size = new System.Drawing.Size(178, 47);
            this.create.TabIndex = 9;
            this.create.Text = "Create room";
            this.create.UseVisualStyleBackColor = false;
            this.create.Click += new System.EventHandler(this.create_Click);
            // 
            // best
            // 
            this.best.BackColor = System.Drawing.Color.Transparent;
            this.best.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("best.BackgroundImage")));
            this.best.Enabled = false;
            this.best.Font = new System.Drawing.Font("Gabriola", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.best.Location = new System.Drawing.Point(416, 458);
            this.best.Name = "best";
            this.best.Size = new System.Drawing.Size(178, 47);
            this.best.TabIndex = 11;
            this.best.Text = "Best Scores";
            this.best.UseVisualStyleBackColor = false;
            this.best.Click += new System.EventHandler(this.best_Click);
            // 
            // quit
            // 
            this.quit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("quit.BackgroundImage")));
            this.quit.Font = new System.Drawing.Font("Gabriola", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quit.Location = new System.Drawing.Point(470, 522);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(69, 50);
            this.quit.TabIndex = 12;
            this.quit.Text = "quit";
            this.quit.UseVisualStyleBackColor = true;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // user
            // 
            this.user.AutoSize = true;
            this.user.BackColor = System.Drawing.Color.Transparent;
            this.user.Font = new System.Drawing.Font("Gabriola", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user.Location = new System.Drawing.Point(12, 12);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(0, 50);
            this.user.TabIndex = 13;
            // 
            // hello
            // 
            this.hello.AutoSize = true;
            this.hello.BackColor = System.Drawing.Color.Transparent;
            this.hello.Font = new System.Drawing.Font("Gabriola", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hello.Location = new System.Drawing.Point(383, 150);
            this.hello.Name = "hello";
            this.hello.Size = new System.Drawing.Size(217, 59);
            this.hello.TabIndex = 14;
            this.hello.Text = "Hello <username>";
            this.hello.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.hello.Visible = false;
            // 
            // signout
            // 
            this.signout.BackColor = System.Drawing.Color.Transparent;
            this.signout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("signout.BackgroundImage")));
            this.signout.Font = new System.Drawing.Font("Gabriola", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signout.Location = new System.Drawing.Point(416, 246);
            this.signout.Name = "signout";
            this.signout.Size = new System.Drawing.Size(178, 47);
            this.signout.TabIndex = 15;
            this.signout.Text = "Sign out";
            this.signout.UseVisualStyleBackColor = false;
            this.signout.Visible = false;
            this.signout.Click += new System.EventHandler(this.signout_Click);
            // 
            // error
            // 
            this.error.AutoSize = true;
            this.error.BackColor = System.Drawing.Color.Transparent;
            this.error.Font = new System.Drawing.Font("Gabriola", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error.ForeColor = System.Drawing.Color.Red;
            this.error.Location = new System.Drawing.Point(450, 204);
            this.error.Name = "error";
            this.error.Size = new System.Drawing.Size(0, 50);
            this.error.TabIndex = 16;
            this.error.Click += new System.EventHandler(this.error_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::TriviaClient.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(327, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(364, 119);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TriviaClient.Properties.Resources._39d43d061a91b5f0266479feb7bf4596;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(994, 594);
            this.Controls.Add(this.signup);
            this.Controls.Add(this.signout);
            this.Controls.Add(this.hello);
            this.Controls.Add(this.user);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.best);
            this.Controls.Add(this.status);
            this.Controls.Add(this.create);
            this.Controls.Add(this.join);
            this.Controls.Add(this.password);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.username);
            this.Controls.Add(this.signin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.error);
            this.Name = "menu";
            this.Text = "Noa and Keren\'s Trivia - menu";
            this.Load += new System.EventHandler(this.menu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button signin;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button signup;
        private System.Windows.Forms.Button join;
        private System.Windows.Forms.Button status;
        private System.Windows.Forms.Button create;
        private System.Windows.Forms.Button best;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.Label user;
        private System.Windows.Forms.Label hello;
        private System.Windows.Forms.Button signout;
        private System.Windows.Forms.Label error;
    }
}

