﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TriviaClient
{
    public partial class game : Form
    {
        Thread th2;
        Thread th1;
        public static Thread th;
        menu _returnTo;
        Socket _socket;
        bool _ans = true;
        string _username;
        string _roomName;
        string _playersNo;
        string _questionsNo;
        string _time;
        int _currTime;
        string _currMsg;
        int _currQue;
        int _id;
        Button[] btns = new Button[4];
        Queue<string> _q;
        int _score;

        public game()
        {
            InitializeComponent();
        }

        public game(menu m, string username, string roomName, string playersNo, string questionsNo, string time, Socket sock, string currMsg)
        {
            _id = 1;
            _currTime = 0;
            _socket = sock;
            _returnTo = m;
            _roomName = roomName;
            _playersNo = playersNo;
            _questionsNo = questionsNo;
            _time = time;
            _username = username;
            _currQue = 1;
            _score = 0;
            _currMsg = currMsg.Substring(3);
            _q = new Queue<string>();

            _socket.ReceiveBufferSize = 8192;

            th1 = new Thread(waitForMsg);
            th1.Start();  

            InitializeComponent();
        }

        /*
        The function receives messages from the server and puts the message in the messages' queue
        */
        private void waitForMsg()
        {
            Byte[] reply = new Byte[1000];
            while (_ans == true)
            {
                _socket.Receive(reply);
                string result = System.Text.Encoding.UTF8.GetString(reply);
                lock (_q)   //No one can access the notification queue
                {
                    _q.Enqueue(result);
                };
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        //event - when button firstAnswer is clicked
        private void button1_Click(object sender, EventArgs e)
        {
            _id = 1;
            answerChose(1, Int32.Parse(_time) - _currTime);
        }

        private void game_Load(object sender, EventArgs e)
        {            
            th2 = new Thread(takeCareOfMsgs);
            th2.Start();
            
            btns[0] = button1;
            btns[1] = button2;
            btns[2] = button3;
            btns[3] = button4;
            user.Text = _username;
            label1.Text = _roomName;
            label2.Text = "1/" + _questionsNo;
            t.Text = _time;
            updateQuestion();   //load the first question
        }

        //event - when the button back is clicked
        private void back_Click(object sender, EventArgs e)
        {
            _socket.Send(Encoding.UTF8.GetBytes("222"));    //send message leave game
            this.Close();
            _returnTo.Show();
        }

        /*
        The function updates the question
        */
        private void updateQuestion()
        {
            //currMsg contains the new message sent by the server
            //cut the message according to the protocol
            int questionLen = Int32.Parse(_currMsg.Substring(0, 3));

            Invoke((MethodInvoker)delegate {que.Text = _currMsg.Substring(3, questionLen);});
            Invoke((MethodInvoker)delegate { que.Location = new Point((this.Width / 2) - (que.Size.Width / 2), que.Location.Y); });
            string[] answers = new string[4];
            int currIndex = 3 + questionLen;

            //update the answers
            for (int i = 0; i < 4; i++)
            {
                int answerLen = Int32.Parse(_currMsg.Substring(currIndex, 3));
                answers[i] = _currMsg.Substring(currIndex + 3, answerLen);
                Invoke((MethodInvoker)delegate { btns[i].Text = answers[i]; });
                Invoke((MethodInvoker)delegate { btns[i].BackColor = Color.White; });                
                currIndex += 3 + answerLen;
            }

            for(int i = 0;i<4;i++)
            {
                Invoke((MethodInvoker)delegate { btns[i].Enabled = true; });    //so the user can click the buttons
            }

            th = new Thread(timeCount); //count the time
            th.Start();
        }
       
        /*
       The function accepts a number and number of bytes, turns the number to String
       and adds zeros at the beginning of the number until the length of the string is equal to the desired number of bytes.
       Input:  number (int), number of bytes (int)
       Output: the new string
       */
        public string ValuePadding(int num, int bytes)
        {
            string str = num.ToString();
            while (str.Length < bytes)
            {
                str = "0" + str;
            }
            return str;
        }

        //The function counts within how long the user answered the question
        private void timeCount()
        {
            try
            {
                for (_currTime = Int32.Parse(_time); _currTime >= 0; _currTime--)
                {
                    System.Threading.Thread.Sleep(1000);    //wait for a second
                    Invoke((MethodInvoker)delegate { t.Text = _currTime.ToString(); });
                }
                answerChose(5, Int32.Parse(_time)); //If the user did not have time to answer
            }
            catch(Exception ex)
            {
                Console.WriteLine("failed!" + ex.Message);
            }
        }

        /*
        The function handles the case where a response is selected
        */
        private void answerChose(int id, int time)
        {
            byte[] reply = new byte[10000];
            for (int i = 0; i < 4; i++)
            {
                Invoke((MethodInvoker)delegate { btns[i].Enabled = false; });   //so the user can not click the buttons
            }

            try
            {
                if (id == 5)    //if the user didn't answered on time
                {
                    for (int i = 0; i < btns.Length; i++)
                    {
                        btns[i].BackColor = Color.Red;
                    }
                }
                else
                {
                    th.Abort(); //stop the time counter
                }

                string msg = "219" + id.ToString() + ValuePadding(time,2);  //send the answer
                _socket.Send(Encoding.UTF8.GetBytes(msg));
            }
            catch (Exception ex)
            {
                Console.WriteLine("sending failed!" + ex.Message);
            }
        }

        //event - when button firstAnswer is clicked
        private void button2_Click(object sender, EventArgs e)
        {
            _id = 2;
            answerChose(2, Int32.Parse(_time) - _currTime);
        }

        //event - when button firstAnswer is clicked
        private void button3_Click(object sender, EventArgs e)
        {
            _id = 3;
            answerChose(3, Int32.Parse(_time) - _currTime);
        }

        //event - when button firstAnswer is clicked
        private void button4_Click(object sender, EventArgs e)
        {
            _id = 4;
            answerChose(4, Int32.Parse(_time) - _currTime);
        }

        /*
        The function handles messages that in the messages queue by the protocol
        */
        private void takeCareOfMsgs()
        {
            string result = "";
            while (_ans == true)
            {
                if (_q.Count != 0)
                {
                    lock(_q){ result = _q.Dequeue(); }; //get the first message in the queue
                    if (result.StartsWith("1200"))  //if the answer the user chose is wrong
                    {
                        Invoke((MethodInvoker)delegate { btns[_id - 1].BackColor = Color.Red; });
                        
                    }
                    else if (result.StartsWith("1201")) //if the answer the user chose is right
                    {
                        _score++;
                        Invoke((MethodInvoker)delegate { btns[_id - 1].BackColor = Color.Green; });
                    }
                    else if (result == "1180")
                    {
                        Console.WriteLine("Failed!");
                    }
                    else if (result.StartsWith("118"))  //if the server send the next question
                    {
                        //update the score
                        Invoke((MethodInvoker)delegate { sc.Text = "score: " + _score.ToString() + "/" + _currQue.ToString(); });
                        System.Threading.Thread.Sleep(800); //so the colors of the answers could be seen
                        _currMsg = result.Substring(3); //update the question
                        _currQue++;
                        Invoke((MethodInvoker)delegate { label2.Text = _currQue.ToString() + "/" + _questionsNo; });
                        Invoke((MethodInvoker)delegate { t.Text = _time; });    //reset the time                         
                        updateQuestion();
                    }
                    else if (result.StartsWith("121"))  //if the game is ended
                    {
                        _ans = false;   //stop the loop
                        //cut the message according to the protocol
                        int usersNo = Int32.Parse(result.Substring(3, 1));
                        string scores = "", info = result.Substring(4), name = "", score = "";
                        int nameLen = 0;
                        for (int i = 0; i < usersNo; i++)
                        {
                            nameLen = Int32.Parse(info.Substring(0, 2));
                            name = info.Substring(2, nameLen);
                            score = info.Substring(2 + nameLen, 2);
                            info = info.Substring(4 + nameLen);
                            scores += "UserName: " + name + "\tScore: " + score + "\n";
                        }
                        MessageBox.Show(scores);    //show the scores
                        Invoke((MethodInvoker)delegate { _returnTo.Show(); });
                        Invoke((MethodInvoker)delegate { this.Close(); });  //close the form
                        th1.Abort();
                        th2.Abort();
                    }
                }
            }
        }

        private void game_FormClosing(object sender, FormClosingEventArgs e)
        {
        }
    }
}
