﻿namespace TriviaClient
{
    partial class personalStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(personalStatus));
            this.user = new System.Windows.Forms.Label();
            this.back = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numGames = new System.Windows.Forms.Label();
            this.numRight = new System.Windows.Forms.Label();
            this.numWrong = new System.Windows.Forms.Label();
            this.timeAvg = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // user
            // 
            this.user.AutoSize = true;
            this.user.BackColor = System.Drawing.Color.Transparent;
            this.user.Font = new System.Drawing.Font("Gabriola", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user.Location = new System.Drawing.Point(12, 12);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(0, 50);
            this.user.TabIndex = 27;
            // 
            // back
            // 
            this.back.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("back.BackgroundImage")));
            this.back.Font = new System.Drawing.Font("Gabriola", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back.Location = new System.Drawing.Point(887, 15);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(100, 60);
            this.back.TabIndex = 26;
            this.back.Text = "back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Image = global::TriviaClient.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(323, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(364, 119);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Gabriola", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(399, 173);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(220, 65);
            this.label1.TabIndex = 28;
            this.label1.Text = "My performances:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Gabriola", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(379, 228);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(240, 65);
            this.label2.TabIndex = 29;
            this.label2.Text = "Number of games - ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Gabriola", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(347, 280);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(316, 65);
            this.label3.TabIndex = 30;
            this.label3.Text = "Number of right answers - ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Gabriola", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(347, 332);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(329, 65);
            this.label4.TabIndex = 31;
            this.label4.Text = "Number of wrong answers - ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Gabriola", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(356, 384);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(304, 65);
            this.label5.TabIndex = 32;
            this.label5.Text = "Average time for answer - ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // numGames
            // 
            this.numGames.AutoSize = true;
            this.numGames.BackColor = System.Drawing.Color.Transparent;
            this.numGames.Font = new System.Drawing.Font("Gabriola", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numGames.ForeColor = System.Drawing.SystemColors.ControlText;
            this.numGames.Location = new System.Drawing.Point(595, 228);
            this.numGames.Name = "numGames";
            this.numGames.Size = new System.Drawing.Size(42, 65);
            this.numGames.TabIndex = 33;
            this.numGames.Text = "0";
            // 
            // numRight
            // 
            this.numRight.AutoSize = true;
            this.numRight.BackColor = System.Drawing.Color.Transparent;
            this.numRight.Font = new System.Drawing.Font("Gabriola", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numRight.ForeColor = System.Drawing.SystemColors.ControlText;
            this.numRight.Location = new System.Drawing.Point(643, 277);
            this.numRight.Name = "numRight";
            this.numRight.Size = new System.Drawing.Size(42, 65);
            this.numRight.TabIndex = 34;
            this.numRight.Text = "0";
            // 
            // numWrong
            // 
            this.numWrong.AutoSize = true;
            this.numWrong.BackColor = System.Drawing.Color.Transparent;
            this.numWrong.Font = new System.Drawing.Font("Gabriola", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numWrong.ForeColor = System.Drawing.SystemColors.ControlText;
            this.numWrong.Location = new System.Drawing.Point(652, 327);
            this.numWrong.Name = "numWrong";
            this.numWrong.Size = new System.Drawing.Size(42, 65);
            this.numWrong.TabIndex = 35;
            this.numWrong.Text = "0";
            this.numWrong.Click += new System.EventHandler(this.label8_Click);
            // 
            // timeAvg
            // 
            this.timeAvg.AutoSize = true;
            this.timeAvg.BackColor = System.Drawing.Color.Transparent;
            this.timeAvg.Font = new System.Drawing.Font("Gabriola", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeAvg.ForeColor = System.Drawing.SystemColors.ControlText;
            this.timeAvg.Location = new System.Drawing.Point(643, 377);
            this.timeAvg.Name = "timeAvg";
            this.timeAvg.Size = new System.Drawing.Size(42, 65);
            this.timeAvg.TabIndex = 36;
            this.timeAvg.Text = "0";
            // 
            // personalStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TriviaClient.Properties.Resources._39d43d061a91b5f0266479feb7bf4596;
            this.ClientSize = new System.Drawing.Size(994, 594);
            this.Controls.Add(this.timeAvg);
            this.Controls.Add(this.numWrong);
            this.Controls.Add(this.numRight);
            this.Controls.Add(this.numGames);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.user);
            this.Controls.Add(this.back);
            this.Controls.Add(this.pictureBox1);
            this.Name = "personalStatus";
            this.Text = "Noa and Keren\'s Trivia - personalStatus";
            this.Load += new System.EventHandler(this.personalStatus_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label user;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label numGames;
        private System.Windows.Forms.Label numRight;
        private System.Windows.Forms.Label numWrong;
        private System.Windows.Forms.Label timeAvg;
    }
}