﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class menu : Form
    {
        string _username;
        Socket _socket;

        public menu()
        {
            Connect("127.0.0.1", 8820); //connect with the server
            InitializeComponent();
        }

        // Synchronous connect using IPAddress to resolve the 
        // host name.
        public void Connect(string host, int port)
        {
            //create socket and make connection with the server
            IPAddress[] IPs = System.Net.Dns.GetHostAddresses(host);

            _socket = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream,
                ProtocolType.Tcp);

            Console.WriteLine("Establishing Connection to {0}",host);
            _socket.Connect(IPs[0], port);
            Console.WriteLine("Connection established");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        //event - when the button signUp is clicked
        private void button1_Click(object sender, EventArgs e)
        {
            signup signupForm = new signup(this, _socket);  //go to the form signUp
            this.Hide();
            signupForm.Show();
        }

        /*
        The function accepts a number and number of bytes, turns the number to String
        and adds zeros at the beginning of the number until the length of the string is equal to the desired number of bytes.
        Input:  number (int), number of bytes (int)
        Output: the new string
        */
        public string ValuePadding(int num, int bytes)
        {
            string str = num.ToString();    //turn the number to string
            while(str.Length < bytes)   //until the string's length is equal to the desired number of bytes
            {
                str = "0" + str;
            }
            return str;
        }

        //event - when the button signIn is clicked
        private void signin_Click(object sender, EventArgs e)
        {
            _username = username.Text;
            string pass = password.Text;

            //the length of the username's length and the length of the password's length should be up to 2 bytes
            //so the username's length and the password's length should be up to 99
            if (_username.Length <= 99 && pass.Length <= 99)
            {
                //According to the protocol, message code = 200
                string msg = "200" + ValuePadding(_username.Length, 2) + _username + ValuePadding(pass.Length, 2) + pass;
                byte[] bytes = new byte[256];
                try
                {
                    _socket.Send(Encoding.UTF8.GetBytes(msg));
                    _socket.Receive(bytes);
                    int response = Int32.Parse(Encoding.UTF8.GetString(bytes));
                    
                    switch (response)
                    {
                        case 1020:  //success
                            hello.Text = "Hello " + _username;
                            hello.Location = new Point((this.Width / 2) - (hello.Size.Width / 2), hello.Location.Y);
                            user.Text = _username;
                            label1.Visible = false;
                            label2.Visible = false;
                            username.Visible = false;
                            password.Visible = false;
                            hello.Visible = true;
                            signup.Visible = false;
                            signout.Visible = true;
                            signin.Visible = false;

                            join.Enabled = true;
                            create.Enabled = true;
                            status.Enabled = true;
                            best.Enabled = true;
                            error.Text = "";
                            break;
                        case 1021:  //if there are wrong details
                            error.Text = "Wrong details";
                            error.Location = new Point((this.Width / 2) - (error.Size.Width / 2), error.Location.Y);
                            break;
                        case 1022:  //if the user is already connected
                            error.Text = "User is already connected";
                            error.Location = new Point((this.Width / 2) - (error.Size.Width / 2), error.Location.Y);    //to center the string
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Connection failed!" + ex.Message);
                }
            }
            else
            {
                error.Text = "Invalid input! too many chars";
            }
        }

        //event - the button joinRoom is clicked
        private void join_Click(object sender, EventArgs e)
        {
            joinroom joinroomForm = new joinroom(this, _username, _socket); //go to the form joinRoom
            this.Hide();
            joinroomForm.Show();
        }

        //event - the button createRoom is clicked
        private void create_Click(object sender, EventArgs e)
        {
            createroom createroomForm = new createroom(this, _username, _socket);   //go to the form createRoom
            this.Hide();
            createroomForm.Show();
        }

        //event - the button quit is clicked
        public void quit_Click(object sender, EventArgs e)
        {
            _socket.Send(Encoding.UTF8.GetBytes("299"));
            //close the application
            Application.Exit();
        }

        //event - the button signOut is clicked
        private void signout_Click(object sender, EventArgs e)
        {
            try
            {
                _socket.Send(Encoding.UTF8.GetBytes("201"));
            }
            catch (Exception ex)
            {
                Console.WriteLine("sending failed!" + ex.Message);
            }

            hello.Text = "";
            user.Text = "";
            label1.Visible = true;
            label2.Visible = true;
            username.Text = "";
            password.Text = "";
            username.Visible = true;
            password.Visible = true;
            hello.Visible = false;
            signup.Visible = true;
            signout.Visible = false;

            join.Enabled = false;
            create.Enabled = false;
            status.Enabled = false;
            best.Enabled = false;
        }

        private void menu_Load(object sender, EventArgs e)
        {
            
        }

        //event - the button personalStatus is clicked
        private void status_Click(object sender, EventArgs e)
        {
            byte[] bytes = new byte[256];
            try
            {
                _socket.Send(Encoding.UTF8.GetBytes("225"));
                _socket.Receive(bytes);
                string response = Encoding.UTF8.GetString(bytes);

                //go to the form personalStatus
                personalStatus personalStatusForm = new personalStatus(_socket, response, this, _username);
                personalStatusForm.Show();
                this.Hide();
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Connection failed!" + ex.Message);
            }
        }

        //event - the button bestScores is clicked
        private void best_Click(object sender, EventArgs e)
        {
            byte[] bytes = new byte[1000];
            try
            {
                _socket.Send(Encoding.UTF8.GetBytes("223"));
                _socket.Receive(bytes);
                string response = Encoding.UTF8.GetString(bytes);
                bestScores bestScoresForm = new bestScores(_socket, response, this, _username); //go to the form bestScores
                bestScoresForm.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Connection failed!" + ex.Message);
            }
        }

        private void error_Click(object sender, EventArgs e)
        {

        }
    }

}
