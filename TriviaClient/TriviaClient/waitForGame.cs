﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class waitForGame : Form
    {
        Thread t1;
        Thread t2;
        menu _returnTo;
        string _username;
        bool _ans = true;
        string _roomName;
        string _playersNo;
        string _questionsNo;
        string _time;
        string _admin;
        Socket _socket;
        Queue<string> _q;

        public waitForGame()
        {
            InitializeComponent();
        }

        public waitForGame(menu m, string username, string roomName, string playersNo, string questionsNo, string time, string admin, Socket sock)
        {
            _socket = sock;
            _admin = admin;
            _returnTo = m;
            _roomName = roomName;
            _playersNo = playersNo;
            _questionsNo = questionsNo;
            _time = time;
            _username = username;
            _q = new Queue<string>();
            
            InitializeComponent();
        }

        /*
        The function handles messages that in the messages queue by the protocol
        */
        private void takeCareOfMsgs()
        {
            string result = "";
            while (_ans == true)
            {
                if (_q.Count != 0)
                {
                    lock (_q) { result = _q.Dequeue(); }; //get the first message in the queue
                    if (result == "1180")
                    {
                        Console.WriteLine("Starting game failed!");
                    }
                    else if (result.StartsWith("118"))  //if the game started
                    {
                        game gameForm = new game(_returnTo, _username, _roomName, _playersNo, _questionsNo, _time, _socket, result);
                        _ans = false;   //stop the loop
                        Invoke((MethodInvoker)delegate { gameForm.Show(); });
                        Invoke((MethodInvoker)delegate { this.Close(); });
                        t1.Abort();
                        t2.Abort();
                    }
                    else if (result.StartsWith("108"))  //if the server send list of users in the room
                    {
                        Invoke((MethodInvoker)delegate { list.Items.Clear(); });                        
                        string username = "";
                        int usernameLen = 0;
                        //cut the message by the protocol
                        int numUsers = Int32.Parse(result[3].ToString());
                        _playersNo = numUsers.ToString();

                        if (numUsers != 0)
                        {
                            string users = (result.Substring(4));

                            for (int i = 0; i < numUsers; i++)
                            {
                                usernameLen = Int32.Parse(users.Substring(0, 2));
                                username = users.Substring(2, usernameLen);
                                Invoke((MethodInvoker)delegate { list.Items.Add(username); });  //add the user to the users' list
                                users = users.Substring(2 + usernameLen);
                            }
                        }
                        Invoke((MethodInvoker)delegate { list.Enabled = false; });  //so no one can click on the list
                    }
                    else if (result.StartsWith("116"))  //if the room is closed
                    {
                        _ans = false;   //stop the loop
                        if (_admin != "")   //if the user is the admin
                        {
                            Invoke((MethodInvoker)delegate { _returnTo.Show(); });
                            Invoke((MethodInvoker)delegate { this.Close(); });
                        }
                        else    //if the user is not the admin
                        {
                            leave.Text = "OK";
                            error.Text = "Admin closed the room";
                            error.Location = new Point((this.Width / 2) - (error.Size.Width / 2), error.Location.Y);
                        }
                    }
                    else if (result.StartsWith("1120")) //if the user left the room
                    {
                        _ans = false;   //stop the loop
                        Invoke((MethodInvoker)delegate { _returnTo.Show(); });
                        Invoke((MethodInvoker)delegate { this.Close(); });
                    }
                }
            }
        }

        /*
        The function receives messages from the server and puts the message in the messages' queue
        */
        private void waitForMsg()
        {
            Byte[] reply = new Byte[1000];
            while (_ans == true)
            {
                _socket.Receive(reply);
                string result = System.Text.Encoding.UTF8.GetString(reply);
                lock (_q)   //No one can access the notification queue
                {
                    _q.Enqueue(result);
                };
            }
        }

        //event - the button closeRoom is clicked
        private void btn_Click(object sender, EventArgs e)
        {
            byte[] reply = new byte[10000];

            try
            {
                _socket.Send(Encoding.UTF8.GetBytes("215"));         
            }
            catch (Exception ex)
            {
                Console.WriteLine("sending failed!" + ex.Message);
            }
        }

        /*
        The function accepts a number and number of bytes, turns the number to String
        and adds zeros at the beginning of the number until the length of the string is equal to the desired number of bytes.
        Input:  number (int), number of bytes (int)
        Output: the new string
        */
        public string ValuePadding(int num, int bytes)
        {
            string str = num.ToString();
            while (str.Length < bytes)
            {
                str = "0" + str;
            }
            return str;
        }

        private void waitForGame_Load(object sender, EventArgs e)
        {
            list.Enabled = false;   //so no one can click on the list
            t1 = new Thread(waitForMsg);
            t1.Start();
            t2 = new Thread(takeCareOfMsgs);
            t2.Start();
            user.Text = _username;
            label1.Text += _roomName;
            label2.Text = "max_number_players: " + _playersNo + "   number_of_questions: " + _questionsNo + "    time_per_question:    " + _time;

            label1.Location = new Point((this.Width / 2) - (label1.Size.Width / 2), label1.Location.Y);
            label2.Location = new Point((this.Width / 2) - (label2.Size.Width / 2), label2.Location.Y);

            if (_admin != "")   //if the user is the admin
            {
                list.Items.Add(_admin);
                //the admin has options like: close the room and start the game
                close.Visible = true;
                start.Visible = true;
            }
            else    //if the user is not the admin
            {
                //a normal player has option to leave the room
                leave.Visible = true;
            }
        }
        
        //event - the button startGame is clicked
        private void start_Click(object sender, EventArgs e)
        {
            byte[] bytes = new byte[1024];
            try
            {
                _socket.Send(Encoding.UTF8.GetBytes("217"));           
            }
            catch (Exception ex)
            {
                Console.WriteLine("Starting game failed!" + ex.Message);
            }
        }

        //event - the button leaveRoom is clicked
        private void leave_Click(object sender, EventArgs e)
        {
            byte[] bytes = new byte[256];
            try
            {
                //if the user is not the admin and the admin closed the room
                if(leave.Text == "OK")
                {
                    _returnTo.Show();
                    this.Hide();
                }
                else //if the user left the room
                {
                    _socket.Send(Encoding.UTF8.GetBytes("211"));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Leaving room failed!" + ex.Message);
            }
        }

        private void waitForGame_FormClosing(object sender, FormClosingEventArgs e)
        {
            _ans = false;   //stop the threads
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void list_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
