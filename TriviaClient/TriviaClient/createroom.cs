﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class createroom : Form
    {
        menu _returnTo;
        string _username;
        Socket _socket;

        public createroom()
        {
            InitializeComponent();
        }    

        public createroom(menu m, string username, Socket sock)
        {
            _returnTo = m;
            _socket = sock;
            _username = username;
            InitializeComponent();
        }

        /*The function gets a string and check if it contains only numbers
         Input: string
         Output:    bool (true if the string contains only numbers, false if not)*/
        private bool checkIfNumber(string str)
        {
            bool ans = true;
            
            for (int i = 0; i < str.Length && ans == true; i++) //all the chars in the string
            {
                if (str[i] != '0' && str[i] != '1' && str[i] != '2' && str[i] != '3' && str[i] != '4' && str[i] != '5' && str[i] != '6' && str[i] != '7' && str[i] != '8' && str[i] != '9')
                {
                    ans = false;    //stop the loop
                }
            }

            return ans;
        }

        //event - the button back is clicked
        private void back_Click(object sender, EventArgs e)
        {
            this.Close();
            _returnTo.Show();   //return to the menu
        }

        private void createroom_Load(object sender, EventArgs e)
        {
            user.Text = _username;        
        }

        /*
        The function accepts a number and number of bytes, turns the number to String
        and adds zeros at the beginning of the number until the length of the string is equal to the desired number of bytes.
        Input:  number (int), number of bytes (int)
        Output: the new string
        */
        public string ValuePadding(int num, int bytes)
        {
            string str = num.ToString();
            while (str.Length < bytes)
            {
                str = "0" + str;
            }
            return str;
        }

        //event - the button create is clicked
        private void create_Click(object sender, EventArgs e)
        {
            string roomName = roomname.Text;
            string playersNumber = playersNo.Text;
            string questionsNumber = questionsNo.Text;
            string timePerQuestion = time.Text;

            if (roomName == "" || playersNumber == "" || questionsNumber == "" || timePerQuestion == "")
            {
                error.Text = "Invalid input! Missing data";
            }
            else if(roomName == "0" || playersNumber == "0" || questionsNumber == "0" || timePerQuestion == "0")
            {
                error.Text = "Invalid input! can't be 0";
            }
            else if (checkIfNumber(playersNumber) == false || checkIfNumber(questionsNumber) == false || checkIfNumber(timePerQuestion) == false)
            {
                error.Text = "Invalid input! Should be a positive number";
            }
            else if(playersNumber.Length != 1)
            {
                error.Text = "Invalid input! Players number should be 1-9";
            }
            else 
            {
                //create a message according to the protocol
                string msg = "213" + ValuePadding(roomName.Length,2) + roomName + playersNumber + ValuePadding(Int32.Parse(questionsNumber),2) + ValuePadding(Int32.Parse(timePerQuestion), 2);
                byte[] reply = new byte[10000];
                string result = "";

                try
                {
                    _socket.Send(Encoding.UTF8.GetBytes(msg));
                    _socket.Receive(reply);
                    result = System.Text.Encoding.UTF8.GetString(reply);

                    if (result.StartsWith("1140"))  //if success
                    {
                        user.Text = _username;
                        //go to the form waitForGame
                        waitForGame waitForGameForm = new waitForGame(_returnTo, _username, roomName, playersNumber, questionsNumber, timePerQuestion, _username, _socket);
                        this.Hide();
                        waitForGameForm.Show();
                    }
                    else if (result.StartsWith("1141")) //if failed
                    {
                        error.Text = "failed";
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("sending failed!" + ex.Message);
                }
            }     
            
            if (error.Text != "")   //if there is any error
            {
                error.Location = new Point((this.Width / 2) - (error.Size.Width / 2), error.Location.Y);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void createroom_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void user_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
